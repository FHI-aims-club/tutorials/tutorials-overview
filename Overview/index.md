---
hide:
  - navigation
---

# Welcome to the FHI-aims Tutorials Overview Page!

Please use this site to navigate through the available FHI-aims tutorials. 

[FHI-aims](https://fhi-aims.org) is a general-purpose, efficient electronic structure code that treats all electrons and potentials without shape approximations and with flexible settings allowing for simple, fast as well as high-precision simulations. In additions to the tutorials, the full code manual as well as the [FHI-aims gitlab](https://aims-git.rz-berlin.mpg.de/) site contain much more and detailed information about the code and its usage. Support for FHI-aims in a graphical interface for materials science, [GIMS](https://gims.ms1p.org/static/index.html), is available online.

For the fastest start, check out our six-minute [video introduction to FHI-aims](https://www.youtube.com/watch?v=bQTOoPepmO0).

## Fundamentals of FHI-aims

This first section is all about fundamental aspects of running FHI-aims simulations for atoms, molecules, solids, and surfaces.

* **[Building the FHI-aims Code ](https://fhi-aims-club.gitlab.io/tutorials/cmake-tutorial/)**

    Like it or not, the first step to running FHI-aims is to have a binary executable that is compiled for the computer on which it is supposed to run. If you already have access to a compiled binary of FHI-aims, you
    can go straight to the next tutorial, [Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/).

* **[Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/)**

    Learn about running FHI-aims for molecules (spin-unpolarized/-polarized) and solids. The syntax of the input files and the structure of the output files is explained. Find out how to request a structure optimization and to request the calculation of band structure and DOS for solids.
    
* **[Beyond the Basics: Complex Simulation Geometries, Efficient SCF Initialization, Charge and Spin](https://fhi-aims-club.gitlab.io/tutorials/FHI-aims-for-transition-metal-oxides)**

    A tutorial that introduces FHI-aims for complex materials with non-trivial atomic and electronic structure. Key concepts include efficient initialization of ionic and spin-polarized solids and how to construct and simulate clusters, here demonstrated for the transition metal oxide Fe$_2$O$_3$. 
    
* **[Slab Calculations and Surface Simulations with FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/slab-calculations-and-surface-simulations-with-fhi-aims)**

    The basic techniques for surface simulations with FHI-aims are introduced. Learn how to construct, run a slab simulation, and extract and understand the relevant numbers from the output file.
    
* **[Scaling in FHI-aims (Scaling of algorithms used in FHI-aims, strategies for big systems)](https://fhi-aims-club.gitlab.io/tutorials/scaling-in-fhi-aims/)**

    Learn about running FHI-aims on a supercomputer and the most important aspects that you should consider when running large-scale systems with many CPUs.
    
* **[Symmetry for Solids](https://fhi-aims-club.gitlab.io/tutorials/symmetry-for-solids)**

    A short tutorial about symmetry-use in solids.

* **[The XDM Dispersion Correction](https://fhi-aims-club.gitlab.io/tutorials/xdm-tutorial)**

    Theory, usage, and three tutorials showcasing the Exchange-hole Dipole Moment (XDM) dispersion correction in FHI-aims. The tutorials cover the use of XDM for modelling molecular dimers, periodic systems, and show how to fit and use XDM for any functional/basis combination not already included in the defaults.
    
* **[Dispersion-Corrected Hybrid DFT](https://fhi-aims-club.gitlab.io/tutorials/dispersion-corrected-hybrid-dft)**

    A tutorial that introduces how to run dispersion-corrected hybrid DFT for the examples of molecular crystals and organic-inorganic perovskites.

## Machine Learning

* **[Electronic Structure Machine Learning with SALTED](https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-with-salted)**

    Symmetry-Adapted Learning of Three-Dimensional Electron Densities (SALTED) method 10.1021/acs.jctc.1c00576 is designed to predict electron densities in periodic systems using a locally and symmetry-adapted representation of the density field.
    
## Beyond DFT methods in FHI-aims

* [**RPA, GW, and BSE for Molecules and Solids**](https://fhi-aims-club.gitlab.io/tutorials/rpa-and-gw-for-molecules-and-solids)
 
    This tutorial focuses on many-body perturbation theory methods "beyond DFT" in FHI-aims. In particular, three main approaches are discussed: 

      1. The GW approximation for quasiparticle properties (i.e., charged excitations such as "electrons" and "holes" in semiconductors); 
      2. The random-phase approximation (RPA) to electron correlation energies, for total energies; 
      3. The Bethe-Salpeter equation (BSE) based on GW, for neutral (e.g., optical) excitation energies (in FHI-aims, currently available for non-periodic simulations of molecules). 

* [**MP2 and Coupled Cluster for Solids (and Molecules) - the Interface `CC-aims` to `CC4S`**](https://periodic-cc-methods-in-fhi-aims-fhi-aims-club-tu-9d435dcb75ed8d.gitlab.io/)

    The interface `CC-aims` in FHI-aims allows to use the coupled cluster code [CC4S](https://manuals.cc4s.org/user-manual/). The tutorial demonstrates the needed steps to get from a FHI-aims calculation to start a calculation with CC4S. CC4S enables the calculation of:
    
    - MP2
    - CCSD

## Motion of atoms

* [**Molecular Dynamics with i-PI**](https://fhi-aims-club.gitlab.io/tutorials/molecular-dynamics-with-i-pi)

    Introduction to running molecular dynamics (MD) through the software package i-PI. Following things are covered:
    
      * How to use FHI-aims together with i-PI
      * Microcanonical Ensemble and Importance of the step size
      * Thermostats
      * Vibrational analysis with i-PI
      * Thermodynamic integration
      * Simulations at constant pressure

* [**Phonons with FHI-vibes and More**](https://fhi-aims-club.gitlab.io/tutorials/phonons-with-fhi-vibes)

    * How to use FHI-aims through FHI-vibes
    * Calculate Phonon properties: Band structure, DOS, Free Energy, ...
    * Lattice expansion (uasi-harmonic approximation)
    * Calculate Born-Effective Charges and non-analytical term correction for polar materials
    * Band gap renormalization

* [**Thermal Transport with FHI-vibes**](https://fhi-aims-club.gitlab.io/tutorials/thermal-transport-with-fhi-vibes)

    * Showcases the ab initio calculation of thermal transport from a non-perturbative method (Green-Kubo theory) and a perturbative method (Boltzmann transport theory) using FHI-vibes.
    * Highlights the need for a non-perturbative method for anharmonic systems, with the strongly anharmonic CuI used as an example.

* [**Ab Initio Simulations of Tip-Enhanced Raman Spectroscopy**](https://github.com/sabia-group/TERS_Tutorial)

    * Allows ab initio simulations of polarizability tensors including a numerical inhomogeneous local hartree potential originally calculated by TDDFT responses from metallic tip-like structures
    * Repository with a how-to example, input files including the local hartree potentials for selected geometries, and a script to create tip-enhanced Raman images with this implementation in FHI-aims can be found in https://github.com/sabia-group/TERS_Tutorial

* [**FHI-aims and MDAnalysis**](https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-mdanalysis)

    * Shows how to use the [MDAnalysis](https://www.mdanalysis.org/) framework with FHI-aims in a few examples
    * Shows how to take a classical MD trajectory and generate structures in FHI-aims format
    * Shows how to cut out a partial geometry segment from a larger system and write out this reduced system

* [**Electron-Phonon Coupling and Electronic Friction Tutorial**](https://fhi-aims-club.gitlab.io/tutorials/electronic-friction)
    * Calculate electronic friction and electron-phonon coupling of molecules adsorbed on metal surfaces for the system CO adsorbed on a Cu(100) surface.

* [**Electronic Conductivity and Mobility from First Principles with the Kubo-Greenwood Formula**](https://fhi-aims-club.gitlab.io/tutorials/kubo-greenwood-formula)

    * Shows how to compute band type conductivites and mobilities using the ab initio Kubo-Greenwood formalism in FHI-aims.
    * Illustrates the necessary and optional input tags that allow to run and configure this type of calculation, as well as how to analyze and rationalize the output.
    * Examples of conductivity and mobility calculations of Al and SrTiO$_3$

* [**Finding Transition States and Minimum Energy Paths with aimsChain**](https://fhi-aims-club.gitlab.io/tutorials/finding-transition-states-with-aimschain)

    * Shows how to use aimsChain together with FHI-aims to find minimum energy paths
    * Examples of ammonia inversion and Li diffusion barriers in solid electrolytes


## Linear Response Methods

* [**Electric field response in FHI-aims using DFPT**](https://fhi-aims-club.gitlab.io/tutorials/electric-field-response)
    * Calculate the response of molecules and crystals to an external homogenous electric field using density functional perturbation theory
    * Calculate the IR and Raman spectra of a benzene molecule and a benzene crystal.

## Ab initio Thermodynamics

* **[Introduction of Ab Initio Thermodynamics and REGC](https://fhi-aims-club.gitlab.io/tutorials/introduction-of-ab-initio-thermodynamics-and-regc/)**

    This tutorial introduces the main concepts of ab initio atomistic thermodynamics and the replica-exchange grand-canonical (REGC) scheme using FHI-aims.

## Embedding

* **[Introduction to QM/MM Embedded-Cluster Calculations](https://fhi-aims-club.gitlab.io/tutorials/using-fhi-aims-for-embedded-cluster-simulations-in-chemshell)**

    This tutorial introduces a user to the workflow for configuring and running a QM/MM embedded-cluster calculation with ChemShell. The tutorial covers software compilation, building the embedded-cluster model, and performing calculations. Embedded links provide the user with access to further examples and information on the ChemShell package.

## FHI-aims in complex workflows

* **[FHI-aims with ASE](https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-with-ase/)**

    This tutorial offers a comprehensive guide on using the FHI-aims code with the ASE (Atomic Simulation Environment) Python package. It covers installation, manipulation of atomic data, setting up and running calculations, and analyzing results. Topics include initializing ASE, reading and writing FHI-aims files, running calculations using ASE calculators, and several examples, including performing nudged elastic band calculations and utilizing the Socket I/O interface. The tutorial is designed for compatibility with FHI-aims version 240507 and ASE version 3.23, noting differences from prior ASE versions.

* **[Taskblaster workflows for high-throughput simulations](https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-with-taskblaster)**

     Taskblaster is a light-weight python framework built on top of ASE for designing and managing high throughput workflows. 
     It allows for executing multiple tasks (e.g. structural relaxation, band structure calculation, postprocessing 
     analysis, etc.) via command-line interface. This is a brief tutorial demonstrating how to perform high-throughput 
     DFT calculations using Taskblaster with FHI-aims. 
 
* **[High-Throughput Workflows with FHI-aims and atomate2](https://fhi-aims-club.gitlab.io/tutorials/workflows-with-atomate2/)**

    [Atomate2](https://materialsproject.github.io/atomate2/index.html) is a Python library built on top of [Pymatgen](https://pymatgen.org) that provides a collection of 
     computational workflows automating first principles calculations. This tutorial gives an overview on 
     how to install and use the `atomate2` package to run a set of predefined workflows with FHI-aims.

* **[Managing Research Data - FHI-aims and AiiDA](https://fhi-aims-club.gitlab.io/tutorials/fhi-aims-and-aiida/)**

    This tutorial gives an overview on how to use the 
    [aiida-ase](https://github.com/aiidaplugins/aiida-ase) package to manage 
    FHI-aims - driven calculations with [AiiDA](http://www.aiida.net/).
    AiiDA is an open-source "Automated interactive infrastructure and Database 
    for computational science". It enables the creation of Python-based 
    workflows, which can automatically be submitted to local and remote machines.
    AiiDA also keeps track of the origin and destination of files and 
    conveniently stores all information in a database.

* **[Simulation Environment for Atomistic and Molecular Modeling - SEAMM](https://molssi-seamm.github.io/getting_started/index.html)**

    SEAMM is a user-friendly software for atomistic and material simulation, 
    which currently supports running ground-state and relaxation calculations 
    with FHI-aims through its `FHI-aims-step` plugin. It has a graphical user
    interface for designing workflows and a web interface to run them and access 
    the results, that helps focus on the science rather than technicalities of 
    using the software. To start using SEAMM, we recommend first go through the 
    [_Getting Started_](https://molssi-seamm.github.io/getting_started/index.html)
    section for SEAMM itself, following by the [tutorial](https://molssi-seamm.github.io/fhi_aims_step/index.html)
    for the FHI-aims plugin.

Other tutorials will appear at this page over time. We also note that further tutorials related to earlier versions of FHI-aims are available from past Hands-On workshop sites, e.g., [the Hands-on DFT and Beyond workshop 2019](https://th.fhi-berlin.mpg.de/meetings/DFT-workshop-2019/index.php?n=Meeting.Program).

## Spectroscopy

* **[Core Level Spectroscopy with ΔSCF](https://fhi-aims-club.gitlab.io/tutorials/core-level-with-delta-scf/)**

    A comprehensive tutorial on how to perform ΔSCF calculations for simulation of x-ray spectroscopy within FHI-aims using force_occupation_basis and force_occupation_projector for periodic and aperiodic systems.

* **[Magnetic Resonance in FHI-aims: Shieldings, Magnetizabilities, J-Couplings, and Spectra](https://fhi-aims-club.gitlab.io/tutorials/shieldings-magnetizabilities-and-j-couplings)**

    A tutorial focused on magnetic resonance calculations for light-element molecules in FHI-aims.

* **[Low-Field NMR Spectra (SLIC and ZULF) from FHI-aims and SPINACH](https://fhi-aims-club.gitlab.io/tutorials/low-field-nmr-slic-and-zulf-spectra-from-fhi-aims-and-spinach/)**

    A tutorial focused on calculating low-field nuclear magnetic resonance spectra with FHI-aims and the SPINACH spin dynamics code.

## Real-Time TDDFT

* **[Real-Time TDDFT and Ehrenfest Dynamics](https://fhi-aims-club.gitlab.io/tutorials/real-time-time-dependent-dft-in-fhi-aims)**

    A tutorial providing an overview on running RT-TDDFT simulations for both isolated and periodic systems within FHI-aims, including a basic review of the theory and all inputs needed to run the simulations. The tutorial also covers running RT-TDDFT with Ehrenfest dynamics and necessary inputs. 

## Nuclear Quantum Effects

* [**Nuclear-Electronic Orbitals**](https://fhi-aims-club.gitlab.io/tutorials/NEO)

    Introduction to modeling Nuclear Quantum effect with the Nuclear-Electronic Orbital (NEO) method with FHI-aims. Following aspects are covered:
    
    * NEO-DFT calculations
        * For Molecules
        * With Periodic Boundary Conditions
        * For Geometry Optimization
    * Real-time NEO TDDFT calculations
